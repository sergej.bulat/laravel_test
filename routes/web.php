<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/','CatalogController@index')->name('home');
Route::get('/search','CatalogController@search')->name('home');
Route::get('category/{id}','CatalogController@showCategory');
Route::post('category','CatalogController@showCategoryPost');
Route::get('product/{id}','CatalogController@showProduct');

Route::group(['prefix'=>'home','middleware' => ['auth']], function ()
{
	Route::get('/','CatalogController@indexHome')->name('home');
    Route::group(['prefix'=>'categories','middleware' => ['auth']], function ()
	{
		Route::get('/','CategoryController@index');
		Route::post('/save','CategoryController@save');
		Route::post('/destroy','CategoryController@destroy');
	});

	Route::group(['prefix'=>'products','middleware' => ['auth']], function ()
	{
		Route::get('/','ProductController@index');
		Route::post('/save','ProductController@save');
		Route::post('/destroy','ProductController@destroy');
	});
});

Route::get('images/{name}', function($name){
	    $path = storage_path('images') . '/' . $name;
	    // dd($path);
	   	if(!File::exists($path)) abort(404);
	   	$file = File::get($path);
	    $type = File::mimeType($path);
	   	$response = Response::make($file, 200);
	    $response->header("Content-Type", $type);
	   	return $response;
	});
