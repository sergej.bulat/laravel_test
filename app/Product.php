<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
	protected $fillable = [
        'name', 'description','category_id','image','price','properties',
    ];
    public function category(){
        return $this->belongsTo('\App\Category','category_id');
    }

    public function categoryName(){
    	if($this->category_id)
    	{
        	$name = Category::find($this->category_id)->name;
        	return $name;
        }
    }

    public function categoryProducts()
    {
        if($this->category_id)
        {
            $products=$this::where('category_id', '=', $this->category_id)
                    ->where('id', '<>', $this->id)->get();
            $result=[];
            if(count($products)<=4)
                return $products;
            $keyArr=array_rand($products->toArray(),4);
            foreach ($keyArr as $item) {
                $result[]=$products[$item];
            }
            return $result;
        }
        return;
    }
    public function params($request){
        $param =  [
                'name' => 'required|max:255|string',
                'description' => 'required|string',
                'category_id' => 'required|integer',
                'image'=>'required|file|image',
                'price' => 'required|integer',
                'properties' => 'required|string',
            ];
        return $param;
    }
}
