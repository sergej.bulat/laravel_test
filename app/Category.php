<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
	protected $fillable = [
        'name','image', 'description',
    ];

	public function countProduct(){
		$count = DB::table('products')->where('category_id', '=', $this->id)->count();
        return $count;
    }

    public function params($request)
    {
		$param =  [
	            'name' => 'required|max:255|string',
	            'image'=>'required|file|image',
	            'description' => 'required|string',
	        ];
		return $param;
	}
}
