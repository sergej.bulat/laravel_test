<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Product;

class ProductController extends Controller
{
   	public function index()
    {
		$products=Product::all();
		// dd($products);
        return view('product.product',compact('products'));
    }

    public function save(Request $request)
    {
        $this->validate($request,$this->product->params($request));
        if(!is_null($request->get('id'))){
            $id=$request->get('id');
            $product_edited=$request->except('_token','id','image');
        }
        else
            $product_edited=$request->except('_token','image');
            
        $file=$request->file('image');
        if($file) 
        {
          $path=storage_path('/images');
          $hash=time();
          $extension = $file->getClientOriginalExtension();
          $file->move($path,$hash.'.'.$extension);
          $product_edited['image']='images/'.$hash.'.'.$extension;
        }

        Model::unguard();
        if(!is_null($request->get('id')))
        {
            $product=Product::find($id);
            $product->update($product_edited);
            $product->save();
        }
        else
        {
            $product_edited['count_views']=0;
            $product=Product::create($product_edited);
        }
        Model::reguard();
        return redirect('/home/products');
    }

    public function destroy(Request $request)
    {
    	// dd($request);
    	$id=$request->get('id');
        Model::unguard();
        $product=Product::find($id)->delete();
        Model::reguard();
        return 'success';
    }
}
