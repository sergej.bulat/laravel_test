<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
		$categories=Category::all();
        return view('category.category',compact('categories'));
    }

    public function save(Request $request)
    {
    	$this->validate($request,$this->category->params($request));
    	if(!is_null($request->get('id'))){
    		$id=$request->get('id');
    		$category_edited=$request->except('_token','id','image');
    	}
    	else
    		$category_edited=$request->except('_token','image');
        $file=$request->file('image');
        if($file) 
        {
          $path=storage_path('/images');
          $hash=time();
          $extension = $file->getClientOriginalExtension();
          $file->move($path,$hash.'.'.$extension);
          $category_edited['image']='images/'.$hash.'.'.$extension;
        }
    	Model::unguard();
        if(!is_null($request->get('id')))
        {
    		$category=Category::find($id);
	    	$category->update($category_edited);
	        $category->save();
	    }
    	else
    		$category=Category::create($category_edited);
        Model::reguard();
        return redirect('/home/categories');
    }

    public function destroy(Request $request)
    {
    	// dd($request);
    	$id=$request->get('id');
        Model::unguard();
        $category=Category::find($id)->delete();
        Model::reguard();
        return 'success';
    }
}
