<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use App\Category;
use App\Product;
use DB;

class CatalogController extends Controller
{
    public function indexHome()
    {
        return view('home');
    }

    public function index()
    {
    	$categories=Category::all();
        return view('categories',compact('categories'));
    }

    public function showCategory($id)
    {
        $category=Category::find($id);
        if($category){
        	$products=Product::where('category_id', '=', $id)->orderBy('count_views', 'desc')->paginate(12);
            return view('category',compact('products','category'));
        }
        return redirect('/');
    }
    public function showCategoryPost(Request $request)
    {
    	$category_id=$request->get('id');
		$sort_id=$request->get('sort');
		switch ($sort_id) {
			case 1:
				$sort="count_views";
				$sort_by="desc";
				break;
			case 2:
				$sort="name";
				$sort_by="asc";
				break;
			case 3:
				$sort="name";
				$sort_by="desc";
				break;
			case 4:
				$sort="price";
				$sort_by="asc";
				break;
			case 5:
				$sort="price";
				$sort_by="desc";
				break;
			default:
				$sort="count_views";
				$sort_by="desc";
				break;
		}
    	$products=Product::where('category_id', '=', $category_id)->orderBy($sort, $sort_by)->paginate(12);
        return view('productall',compact('products','category_id','sort_id'));
    }

    public function showProduct($id)
    {
        

    	$product=Product::find($id);
        if($product){
            $category=Category::find($product->category_id);
        	$count_views=$product->count_views;
            $product_edited['count_views']=$count_views+1;
        	Model::unguard();
        	$product->update($product_edited);
        	$product->save();
        	Model::reguard();
            return view('product',compact('product','category'));
        }
        return redirect('/');
    }

    public function search()
    {
    	$search=Input::get('search');
    	// dd($search);
    	$query=htmlspecialchars(trim($search));
    	if(!empty($query)){
	    	$search_products=$this->product::whereRaw(
	            "name like ? OR 
	            description like ? OR 
	            properties like ? OR 
	            price like ?", ['%'.$query.'%','%'.$query.'%','%'.$query.'%','%'.$query.'%'])->paginate(3);
	        return view('search',compact('query','search_products'));
    	}
    	return redirect('/');
    }
}
