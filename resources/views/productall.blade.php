<label for="sort">Sorting by: </label>
	<select id="sort" onchange="sort()">
		<option value="1">Most Popular</option>
		<option value="2">Name asc</option>
		<option value="3">Name desc</option>
		<option value="4">Price asc</option>
		<option value="5">Price desc</option>
	</select>
	<br><br>
	@if(count($products))
		<ul class="list-unstyled row">
			@foreach($products as $product)
					<li class="col-md-3 col-xs-12 col-sm-4 text-center">
						<a href="/product/{{$product->id}}">
							<img width="150px" height="150px" class="img-circle" src="/{{ $product->image }}">
						</a>
						<a href="/product/{{$product->id}}">
							<p>{{ $product->name }}</p>
						</a>
						<p>{{ $product->price }}</p>
						<p><a href="/product/{{$product->id}}">Read more</a></p>
					</li>
			@endforeach
		</ul>
	@else
		<h4>No products in this category</h4>
	@endif
<div class="row text-center">{{$products->links()}}</div>
<script type="text/javascript">
	j(function() {
		j('input, select').styler();
	});
</script>