@extends('welcome')
@section('container')
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="/category/{{$category->id}}">Catagory: {{$category->name}}</a></li>
  <li class="active">Product: {{$product->name}}</li>
</ol>
<h3>Catagory: {{$category->name}}</h3>
<ul class="list-unstyled">
	<li class="text-center row">
		<img width="250px" height="250px" class="img-circle" src="/{{ $product->image }}">
	</li>
	<li class="row">
		<label class="col-md-2" for="product-name">Name: </label>
		<span class="col-md-10 product-name">{{ $product->name }}</span>
	</li>
	<li class="row">
		<label class="col-md-2" for="product-category">Category: </label>
		<span class="col-md-10 product-category">{{ $product->categoryName()}}</span>
	</li>
	<li class="row">
		<label class="col-md-2" for="product-price">Price: </label>
		<span class="col-md-10 product-price">{{ $product->price }}</span>
	</li>
	<li class="row">
		<label class="col-md-2" for="product-description">Description: </label>
		<span class="col-md-10 product-description">{{ $product->description }}</span>
	</li>
	<li class="row">
		<label class="col-md-2" for="product-properties">Properties: </label>
		<span class="col-md-10 product-properties">{{ $product->properties }}</span>
	</li>
</ul>
<div class="panel panel-default">
	<div class="panel-body">
	<ul class="list-unstyled row">
	@foreach($product->categoryProducts() as $item)
			<li class="col-md-3 col-xs-12 col-sm-4 text-center">
				<a href="/product/{{$item->id}}">
					<img width="150px" height="150px" class="img-circle" src="/{{ $item->image }}">
				</a>
				<a href="/product/{{$item->id}}">
					<p>{{ $item->name }}</p>
				</a>
				<p>{{ $item->price }}</p>
				<p><a href="/product/{{$item->id}}">Read more</a></p>
			</li>
	@endforeach
</ul>
    </div>
</div>
@endsection