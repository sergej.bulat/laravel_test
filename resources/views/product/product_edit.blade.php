<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Product</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="/home/products/save" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="">
            <input type="hidden" name="id" value="{{ $product->id or old('id') }}">
            <div class="form-group">
                <label for="name">Name</label>
                <br>
                <input class="form-control" type="text" placeholder="Name" name="name" value="{{ $product->name or old('name')}}">`
                @if($errors->has('name'))
                    <span class="help-block">
                        {{$errors->first('name')}}
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <br>
                <textarea class="form-control" name="description" placeholder="Description">{{ $product->description or old('description')}}</textarea>
                @if($errors->has('description'))
                    <span class="help-block">
                        {{$errors->first('description')}}
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label>Image</label>
                <br>
                <input class="form-control" type="file" placeholder="Image" name="image" value="{{ $product->image or old('image')}}">
                @if($errors->has('image'))
                    <span class="help-block">
                        {{$errors->first('image')}}
                    </span>
                @endif
            </div>
            <select class="form-control" name="category_id">
                <option disabled selected>Please select parent category</option>
                @foreach($categories = App\Category::all(); as $category)
                    <option value="{{$category->id}}"
                    @if((isset($product->category_id) && $product->category_id!=null && $product->category_id==$category->id) || old('category_id'))
                    selected
                    @endif
                    >
                        {{$category->name}}
                    </option>
                @endforeach
                @if($errors->has('category_id'))
                    <span class="help-block">
                        {{$errors->first('category_id')}}
                    </span>
                @endif
                </select>
            <div class="form-group">
                <label for="price">Price</label>
                <br>
                <input class="form-control" type="text" placeholder="Price" name="price" value="{{ $product->price or old('price')}}">
                @if($errors->has('price'))
                    <span class="help-block">
                        {{$errors->first('price')}}
                    </span>
                @endif
            </div>
            <div class="form-group">
                <label for="properties">Properties</label>
                <br>
                <textarea class="form-control" name="properties" placeholder="Properties">{{ $product->properties or old('properties')}}</textarea>
                @if($errors->has('properties'))
                    <span class="help-block">
                        {{$errors->first('properties')}}
                    </span>
                @endif
            </div>
        </div>
        <div class="">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
</div>