@extends('layouts.app')
@section('content')
@include('product.product_edit')

<div class="container">
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="/home">Home</a></li>
        <li role="presentation"><a href="/home/categories">Categories</a></li>
        <li role="presentation" class="active"><a href="/home/products">Products</a></li>
    </ul>
</div>
<div class="container">
    <div class="row pull-left">
        <a class="btn btn-link" href="/home"><-Back</a>
    </div>
    <div class="row pull-right">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="newProduct()">
            Add new product
        </button>
    </div>
    <hr>
    <div class="panel panel-default row">

    @if(count($products))
        <table id="product" class="table table-bordered" cellspacing="0" width="100%" data-toggle="table"
       data-sort-name="stargazers_count"
       data-sort-order="desc">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Properties</th>
                    <th>Updated At</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Properties</th>
                    <th>Updated At</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach($products as $product)
                    <tr class="product_{{ $product->id }}">
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->categoryName()}}</td>
                        <td><img src="/{{ $product->image }}" width="70px"/></td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->properties }}</td>
                        <td>{{ $product->updated_at }}</td>
                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="editProduct({{ $product }})">
                                Edit
                            </button>
                            <button class="btn btn-danger" onclick="destroy({{ $product->id }})">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <h2>NO data to show</h2>
        @endif

    </div>
</div>


<script type="text/javascript">
    j(function() {
        j('#product').DataTable();
    });

    function destroy(id)
    {
        var url='/home/products/destroy';
        var token = j("input[name='_token']").val();
        $.ajax({
            url: url,
            type:'post',
            data:{id:id,_token:token},
            success: function(data){
                if(data=='success')
                    j(".product_"+id).html("");
            }
        });
    }
    function editProduct(product)
    {
        j(".modal-title").text('Edit Product');
        j(".modal input[name='id']").val(product.id);
        j(".modal input[name='name']").val(product.name);
        j(".modal textarea[name='description']").text(product.description);
        j(".modal select[name='category_id']").val(product.category_id);
        j(".modal input[name='price']").val(product.price);
        j(".modal textarea[name='properties']").val(product.properties);
    }
    function newProduct()
    {
        j(".modal input[name='id']").val('');
        j(".modal input[name='name']").val('');
        j(".modal textarea[name='description']").text('');
        j(".modal select[name='category_id']").val('');
        j(".modal input[name='price']").val('');
        j(".modal textarea[name='properties']").val('');
    }
</script>
@endsection