@extends('welcome')
@section('container')
<h3>Categories</h3>
<ul class="list-unstyled row">
    @foreach($categories as $category)
            <li class="col-md-3 col-xs-12 col-sm-4 text-center">
                <a href="/category/{{$category->id}}">
                    <img width="220px" height="220px" class="img-circle" src="/{{ $category->image }}">
                </a>
                <a href="/category/{{$category->id}}">
                    <p>{{ $category->name }}</p>
                </a>
                <p><a href="/category/{{$category->id}}">Read more</a></p>
            </li>
    @endforeach
</ul>
@endsection