@extends('welcome')
@section('container')
@if(!empty($search_products->toArray()['data']))
<h4>Products</h4>
<ul class="list-unstyled row">
	@foreach($search_products as $product)
			<li class="col-md-3 col-xs-12 col-sm-4 text-center">
				<a href="/product/{{$product->id}}">
					<img width="150px" height="150px" class="img-circle" src="/{{ $product->image }}">
				</a>
				<a href="/product/{{$product->id}}">
					<p>{{ $product->name }}</p>
				</a>
				<p>{{ $product->price }}</p>
				<p><a href="/product/{{$product->id}}">Read more</a></p>
				<div class="panel panel-default">
					<div class="row">
						<h4>Category:</h4>
						<a href="/category/{{$product->category_id}}">{{$product->categoryName()}}</a>
					</div>
				</div>
			</li>
	@endforeach
</ul>
<div class="row text-center">{{$search_products->links()}}</div>
@else
	<h2>Not found "{{$query}}"</h2>
@endif
@endsection