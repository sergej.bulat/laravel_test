@extends('welcome')
@section('container')
<ol class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li class="active">Catagory: {{$category->name}}</li>
</ol>
<h3>Catagory: {{$category->name}}</h3>
@if(isset($category))
	<input type="hidden" id="category_id" name="category_id" value="{{ $category->id }}">
@endif
@if(count($products))
<div class="category">
	@include('productall', $products)
</div>
@else
        <h4>No products in this category</h4>
@endif
@endsection

<script type="text/javascript">
	function sort()
	{
		var sort=j('#sort').val();
		var category_id=j('#category_id').val();
		var url='/category';
        var token = j("input[name='_token']").val();
        j.ajax({
            url: url,
            type:'post',
            data:{
            	id:category_id,
            	sort:sort,
            	_token:token
            },
            success: function(data){
                j(".category").html(data);
                j("#sort").val(sort);
                j('input, select').trigger('refresh');
            }
        });
	}
</script>