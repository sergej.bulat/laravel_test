@extends('layouts.app')

@section('content')
<div class="container">
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="/home">Home</a></li>
        <li role="presentation"><a href="/home/categories">Categories</a></li>
        <li role="presentation"><a href="/home/products">Products</a></li>
    </ul>
</div>
<div class="container">
        <div class="panel panel-default">
            <div class="panel-heading"><a href="/home/categories">Categories</a></div>
            <div class="panel-body">
                Categories count : {{App\Category::all()->count()}}
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><a href="/home/products">Products</a></div>
            <div class="panel-body">
                Products count : {{App\Product::all()->count()}}
            </div>
        </div>
</div>
@endsection