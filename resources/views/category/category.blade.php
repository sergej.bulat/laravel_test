@extends('layouts.app')
@section('content')
@include('category.category_edit')
<div class="container">
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="/home">Home</a></li>
        <li role="presentation" class="active"><a href="/home/categories">Categories</a></li>
        <li role="presentation"><a href="/home/products">Products</a></li>
    </ul>
</div>
<div class="container">
    <div class="row pull-left">
        <a class="btn btn-link" href="/home"><-Back</a>
    </div>
    <div class="row pull-right">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="newCategory()">
            Add new category
        </button>
    </div>
    <hr>
    <div class="panel panel-default row">
    @if(count($categories))
        <table id="category" class="table table-bordered" cellspacing="0" width="100%" data-toggle="table"
       data-sort-name="stargazers_count"
       data-sort-order="desc">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Count products</th>
                    <th>Updated At</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Description</th>
                    <th>Count products</th>
                    <th>Updated At</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach($categories as $category)
                    <tr class="category_{{ $category->id }}">
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name }}</td>
                        <td><img src="/{{ $category->image }}" width="70px"/></td>
                        <td>{{ $category->description }}</td>
                        <td>{{ $category->countProduct() }}</td>
                        <td>{{ $category->updated_at }}</td>
                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="editCategory({{ $category }})">
                                Edit
                            </button>
                            <button class="btn btn-danger" onclick="destroy({{ $category->id }})">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <h2>NO data to show</h2>
        @endif

    </div>
</div>

<script type="text/javascript">
    j(function() {
        j('#category').DataTable();
    });

    function destroy(id)
    {
        var url='/home/categories/destroy';
        var token = $("input[name='_token']").val();
        $.ajax({
            url: url,
            type:'post',
            data:{id:id,_token:token},
            success: function(data){
                if(data=='success')
                    $(".category_"+id).html("");
            }
        });
    }
    function editCategory(category)
    {
        j(".modal-title").text('Edit Product');
        j(".modal input[name='id']").val(category.id);
        j(".modal input[name='name']").val(category.name);
        j(".modal textarea[name='description']").text(category.description);
    }
    function newCategory()
    {
        j(".modal input[name='id']").val('');
        j(".modal input[name='name']").val('');
        j(".modal textarea[name='description']").text('');
    }
</script>
@endsection