<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Create Category</h4>
      </div>
      <div class="modal-body">
	<form method="post" action="/home/categories/save" enctype="multipart/form-data">
	    {{csrf_field()}}
	    <div class="">
	    	<input type="hidden" name="id" value="{{ $category->id or old('id') }}">
	        <div class="form-group">
	            <label>Name</label>
	            <br>
	            <input class="form-control" type="text" placeholder="Name" name="name" value="{{ $category->name or old('name')}}">
	            @if($errors->has('name'))
                	<span class="help-block">
                		{{$errors->first('name')}}
                	</span>
            	@endif
	        </div>
	        <div class="form-group">
                <label>Image</label>
                <br>
                <input class="form-control" type="file" placeholder="Image" name="image" value="{{ $product->image or old('image')}}">
                @if($errors->has('image'))
                	<span class="help-block">
                		{{$errors->first('image')}}
                	</span>
            	@endif
            </div>
	        <div class="form-group">
	            <label>Description</label>
	            <br>
	            <textarea class="form-control" name="description" placeholder="Description">{{ $category->description or old('description')}}</textarea>
	            @if($errors->has('description'))
                	<span class="help-block">
                		{{$errors->first('description')}}
                	</span>
            	@endif
	        </div>
	    </div>
	    <div class="">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	    </div>
	</form>
      </div>
    </div>
  </div>
</div>